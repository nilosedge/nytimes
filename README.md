# New York Times 7 Experiments

This repo contains the code for running 7 experiments against nytimes API primarily using spacy to glean information about the articles found in the archive metadata.

# Getting Started

These instructions will get you a copy of data and running of the experiments


# Contents

-  [Installing](#installing)
   *  [Python Setup](#python-setup)
-  [Running](#running)
   *  [Download Metadata](#download-metadata)
-  [Experiments](#experiments)
   * [Experiment 1](#experiment-1)
   * [Experiment 2](#experiment-2)
   * [Experiment 3](#experiment-3)
   * [Experiment 4](#experiment-4)
   * [Experiment 5](#experiment-5)
   * [Experiment 6](#experiment-6)
   * [Experiment 7](#experiment-7)
-  [Submitting Data](#submitting-data) 

# Installing

## Installing all the dependencies

```bash
> pip3 install virtualenv
> python3 -m virtualenv venv
> source venv/bin/activate
(venv) >
```

```bash
(venv) > pip install -r requirements.txt
```

```bash
(venv) > python -m spacy download en_core_web_lg
```

```bash
(venv) > python
>>> import nltk
>>> nltk.download('punkt')
```

# Download Metadata

```bash
(venv) > python download.py <NYTIMES_API_KEY>
```
This download process will take about 4 hours its just over 2000 files and you can only download them one per 7 seconds. This downloads the archive metadata for all articles from Sep 1851 to Sep 2022.

```bash
(venv) > python process_headlines.py
```
This will process all the json files extracting the headlines, running them through spacy and storing the information into data/*.spacy DocBin files 

#Experiments

## Experiment 1

### Question: Which Geopolitical entities are most found in the headlines of the New York times that are not US based, top 5.

#### Method

Feed spacy all headlines from 1850's to 2022 and track frequency counts of GPE labels on named entities.

#### Conclusion

Based on the following graph, it would appear that the New York times headlines tend to cover london, paris, france, britian, and japan. What is also interesting is how events in those locations casue spikes in the headline count during the month that it happens.

[<img src="Exp1/ConclusionGraph.png">](Exp1/ConclusionGraph.png)

#### Process

```bash
(venv) > python process_entity_counts.py
```

This will create pickle files for each of the labels from `nlp.get_pipe('ner').labels`
See the html directory for generated results

See the html directory for [generated results](Exp1/html)

```bash
(venv) > jupyter notebook Exp1/Exp1.ipynb
```
This notebook uses the entity counts that were processed and show counts based on the different type of entities.


## Experiment 2

### Question: The New York times has been accused of being bias towards favoring democrats and not favoring to republicans, is this true or false?

#### Method

Using spacy we will create a text matcher looking for "republicans" and "democrats". Then we will loop through all headlines creating docs for only headlines that match the two words. For every document found we are going to compute sentament (SpacyTextBlob) on the document and credit it to either "republicans", "democrats" or both. Then at the end of the month compute the average sentament score for each. Then we take a 10 year (120 month) moving average to see the sentament over time for both "republicans" and "democrats".

#### Conclusion

In the graph below we see the republicans sentament in red and the democrats sentament in blue. We see that over periods of time there is clear bias toward one group vs the other, however long term its not as clear which is more favorable than the other. This graph does show that republicans go much lower than democrats. This graph does point to a slight bias toward favoring democrats over republicans.

[<img src="Exp2/ConclusionGraph1.png">](Exp2/ConclusionGraph1.png)

A better explanation might be to take the democrats score - the republicans score. In theory the line should hover around 0 which would suggest there is no bias. When the line is above 0 the democrats are being favored vs below the 0 line the republicans are being favored. In the graph below we see that the line does spend the majority of the time above the 0 line which would indicate that over the last 150 years the New York times does favor democrats more than republicans.

[<img src="Exp2/ConclusionGraph2.png">](Exp2/ConclusionGraph2.png)

#### Process

```bash
(venv) > python process_sentiment.py
```
This will create a pickle file that holds sentiment for democrats vs republicans

```bash
(venv) > jupyter notebook Exp2/Exp2.ipynb
```

This takes all headlines where the word democrats or the word republicans appear and calculate the sentiment via SpacyTextBlob as positive or negative. The blue line is for democrats and the red is for republicans, and is the running average for the average sentiment for each month.

See the html directory for [generated results](Exp2/html)

## Experiment 3

### Question: Does the New York time write subjective headlines?

#### Method

Take all headline data run through spacy and then run the subjective / neutral classifier on them: [cffl/bert-base-styleclassification-subjective-neutral](https://huggingface.co/cffl/bert-base-styleclassification-subjective-neutral), then take the average subjective score for the month.

#### Conclusion

[<img src="Exp3/ConclusionGraph.png">](Exp3/ConclusionGraph.png)

#### Process

```bash
(venv) > python process_subjectiveness.py
```
This will create pickle file that holds a data frame of subjectivity for all months.

```bash
(venv) > jupyter notebook Exp3/Exp3.ipynb
```

The graph will show how the average subjectivity by month. 

See the html directory for [generated results](Exp3/html)

## Experiment 4

### Question:
#### Method
#### Conclusion
#### Process


## Experiment 5

### Question: Are headlines that talk about adventists similar to each other or different.

#### Method

Feed all headlines through spacy looking for two words (adventist or adventists). This list of documents if fairly small ~800. Then we compute similarity from each document to each other document N x M, and look at the values near 1 and near 0.

#### Conclusion

The two most different headlines are:

[Adventist Missions Increase.] and [ADVENTISTS REPORT $10,556,633 IN GIFTS]

We see that these two topics are very different. Missions (money out) vs church income (money in). 

The two most similar headlines are:

[THE ADVENTISTS DISAPPIONTED] and [THE ADVENTISTS WEAKENING]

These two headlines are similar, but makes for more inquery about why they were written.

#### Process

```bash
(venv) > python process_adventist.py
```
This will create pickle file that holds similar documents for the words adventist and adventists.

```bash
(venv) > jupyter notebook Exp6/Exp6.ipynb
```

This notebook will spit out the top headlines that are similar to each other and the ones that are least simular to each other.

## Experiment 6

### Question: Which adjective is used to complement the phase the most in the headlines.

#### Method

Run the headlines through spacy and look at the DEP labels specifically the "acomp" (adjective complement) label. To see which agjective is most used in the headlines.

#### Conclusion

We find that the two most used adjective complement are "dead" and "married". This semmed to be more related to what the paper choose to report on. As we see marrages were reported more from about 1930 to 1993, we also find the two words engaged and bride during the same timeframe. Where as dead starts in the 1930 and continues on to present day, and seems to spkie during crisis events.

[<img src="Exp6/ConclusionGraph.png">](Exp6/ConclusionGraph.png)

#### Process

```bash
(venv) > python process_pos.py
```
This will create pickle files that hold counts for all the dependencies, by month and generate html graphs for each.

```bash
(venv) > jupyter notebook Exp6/Exp6.ipynb
```

Each graph will show how the dependencies and the counts by month. 

See the html directory for [generated results](Exp6/html)

## Experiment 7

### Question: Which verbs are most used in the headlines. Which actions are the headlines most reporting about.

#### Method

Run the headlines through the spacy and compute frequency counts for all parts of speech (POS). And look at the verbs that the headlines use.

#### Conclusion

We find that both "says" and "reports" are most used. However the "reports" are part of reports on the market, which were only done for a period of time, starting in 1980 and ending in 1996. Where as for the word "says" it was more used starting around 1900 and trailing off around 1970. It would seem that what people "says" was most important to the readership in the 1930's where as today less emphasis is placed on what people say.

[<img src="Exp7/ConclusionGraph.png">](Exp7/ConclusionGraph.png)

#### Process

```bash
(venv) > python process_pos.py
```
This will create pickle files that holds counts for all the part of speech, by month and generate html graphs for each.

```bash
(venv) > jupyter notebook Exp7/Exp7.ipynb
```

Each graph will show how the parts of speech and the counts by month.

See the html directory for [generated results](Exp7/html)
