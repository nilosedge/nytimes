import json
import os
import time
import pickle
import datetime
import nltk # nltk.download('punkt')
import torch
from transformers import pipeline
from transformers import AutoTokenizer, AutoModelForSequenceClassification

from multiprocessing import Pool
import pandas as pd

import spacy
from spacy.tokens import Doc
from spacy.tokens import DocBin
from spacy.matcher import Matcher

from collections import Counter

tokenizer = AutoTokenizer.from_pretrained("cffl/bert-base-styleclassification-subjective-neutral")
model = AutoModelForSequenceClassification.from_pretrained("cffl/bert-base-styleclassification-subjective-neutral")
classify = pipeline(task="text-classification", model="cffl/bert-base-styleclassification-subjective-neutral", top_k=None)

nlp = spacy.load("en_core_web_lg")
nlp.max_length = 2500000

Doc.set_extension("month_key", default=None)

def process_function(key):

	start_time = time.time()
	filename = "data/" + key + ".subject.pickle"

	if os.path.isfile(filename):
		print("File Exists: ", key)
		return

	print("Process starting: ", key)

	#print("Loading JSON File: ", filename)		   
	doc_bin = DocBin().from_disk("data/" + key + ".spacy")
	#print("JSON Data Loaded: ", key)

	bias_counts = None
	count = 0
	neutral_score = 0
	subjective_score = 0
	texts = []
	for doc in doc_bin.get_docs(nlp.vocab):
		if len(doc) < 512:
			texts.append(doc.text)

	data = classify(texts)

	for lst in data:
		for label_score in lst:
			if label_score["label"] == "NEUTRAL":
				neutral_score += label_score["score"]
			if label_score["label"] == "SUBJECTIVE":
				subjective_score += label_score["score"]
		count = count + 1

	ret = { "month_key": key, "subjective": subjective_score/count, "neutral": neutral_score/count }
	with open(filename, 'wb') as handle:
		pickle.dump(ret, handle, protocol=pickle.HIGHEST_PROTOCOL)
	end_time = time.time()
	print(key, " finished processing: ", (end_time - start_time), " to process ", len(texts), " texts ", (len(texts) / (end_time - start_time)), "t/s")

if __name__ == '__main__':

	p = Pool()

	jobs = []

	for line in open('file_index', 'r').readlines():
		words = nltk.word_tokenize(line.strip().replace("/", " ").replace("_", " ").replace(".", " "))
		key = str(datetime.date(int(words[1]), int(words[2]), 1))
		jobs.append(key)

	time.sleep(2)

	results = p.map(process_function, jobs)
	p.close()
	p.join()

	print("Jobs finished")
