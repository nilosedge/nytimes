import json
import time
import datetime
import nltk # nltk.download('punkt')

from multiprocessing import Pool
import pandas as pd

import spacy
from spacy.tokens import Doc
from spacy.tokens import DocBin
from spacy.matcher import Matcher

from collections import Counter

nlp = spacy.load("en_core_web_lg")
nlp.max_length = 2500000

matcher = Matcher(nlp.vocab)
pattern = [{"LOWER": "no"}, {"LOWER": "title"}]
pattern2 = [{"LOWER": "topic"}, {"LOWER": "of"}, {"LOWER": "the"}, {"LOWER": "day"}]
matcher.add("Filtered", [pattern, pattern2])


Doc.set_extension("month_key", default=None)

def process_function(key):

	print("Process starting: ", key)
	#print("Loading JSON File: ", filename)		   
	doc_bin = DocBin().from_disk("data/" + key + ".spacy")
	#print("JSON Data Loaded: ", key)

	word_count_by_label = {}
	for doc in doc_bin.get_docs(nlp.vocab):
		matches = matcher(doc)
		#print(matches)
		if not matches:
			for ent in doc.ents:
				word = ent.text.lower()
				if ent.label_ not in word_count_by_label:
					word_count_by_label[ent.label_] = []

				word_count_by_label[ent.label_].append(ent.text.lower())
				#print(ent.label_)
				
	word_count = {}

	for label in word_count_by_label:
		word_freq = Counter(word_count_by_label[label])
		common_words = word_freq.most_common()

		word_count[label] = {}
		word_count[label]["month_key"] = key
		for word in common_words:
			if word[1] > 1:
				word_count[label][word[0]] = word[1]
	#print(str(key), ": ", common_words)

	return word_count


if __name__ == '__main__':

	p = Pool(8)

	jobs = []

	for line in open('file_index', 'r').readlines():
		words = nltk.word_tokenize(line.strip().replace("/", " ").replace("_", " ").replace(".", " "))
		key = str(datetime.date(int(words[1]), int(words[2]), 1))
		print("Processing file: ", key)
		jobs.append(key)

	time.sleep(2)

	results = p.map(process_function, jobs)
	p.close()
	p.join()

	dfs = {}
	print("Putting results into data frame")
	for result in results:
		for key in result:
			if key not in dfs:
				dfs[key] = []
			dfs[key].append(pd.DataFrame([result[key]]))

	for key in dfs:
		print("Key: ", key)
		print("Concatenating results")
		df = pd.concat(dfs[key], ignore_index = True)
		
		print("Sorting the results")
		df = df.sort_values(by=["month_key"], ascending = True)

		print("Seting the index")
		df = df.set_index('month_key')

		print("Removing zeros")
		df = df.fillna(0)
	
		print("Saving results to pickle")
		df.to_pickle("pickles/" + key + "_entity_data.pickle")

	print("Jobs finished")
