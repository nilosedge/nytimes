import json
import time
import datetime
import nltk # nltk.download('punkt')

from multiprocessing import Pool
import pandas as pd

import spacy
from spacy.tokens import Doc
from spacy.tokens import DocBin
from spacy.matcher import Matcher

from collections import Counter

nlp = spacy.load("en_core_web_lg")
nlp.max_length = 2500000

matcher = Matcher(nlp.vocab)
pattern = [{"LOWER": "adventist"}]
pattern2 = [{"LOWER": "adventists"}]
matcher.add("FindAdventist", [pattern, pattern2])

Doc.set_extension("month_key", default=None)

def process_function(key):

	print("Process starting: ", key)
	#print("Loading JSON File: ", filename)		   
	doc_bin = DocBin().from_disk("data/" + key + ".spacy")
	#print("JSON Data Loaded: ", key)

	adventist_docs = []

	for doc in doc_bin.get_docs(nlp.vocab):
		matches = matcher(doc)
		if matches:
			adventist_docs.append(doc)

	return adventist_docs

if __name__ == '__main__':

	p = Pool()

	jobs = []

	for line in open('file_index', 'r').readlines():
		words = nltk.word_tokenize(line.strip().replace("/", " ").replace("_", " ").replace(".", " "))
		key = str(datetime.date(int(words[1]), int(words[2]), 1))
		print("Processing file: ", key)
		jobs.append(key)

	time.sleep(2)

	results = p.map(process_function, jobs)
	p.close()
	p.join()

	all_docs = []
	for result in results:
		for doc in result:
			all_docs.append(doc)

	doc_bin = DocBin(docs=all_docs, store_user_data = True)
	doc_bin.to_disk("data/adventists.spacy")

	print("Jobs finished")
