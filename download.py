import urllib.request
import time
import sys

index_file = open("file_index", "w")

for year in range(1851, 2023):
	for month in range(1, 13):
		url = "https://api.nytimes.com/svc/archive/v1/" + str(year) + "/" + str(month) + ".json?api-key=" + sys.argv[1]
		outfile = "data/" + str(year) + "_" + str(month) + ".json"
		print("Downloading: ", outfile)
		urllib.request.urlretrieve(url, outfile)
		index_file.write(outfile + "\n")
		index_file.flush()
		time.sleep(7)
