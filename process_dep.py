import json
import time
import datetime
import nltk # nltk.download('punkt')

from multiprocessing import Pool
import pandas as pd

import spacy
from spacy.tokens import Doc
from spacy.tokens import DocBin

from collections import Counter

nlp = spacy.load("en_core_web_lg")
nlp.max_length = 2500000

Doc.set_extension("month_key", default=None)

def process_function(key):

	print("Process starting: ", key)
	#print("Loading JSON File: ", filename)		   
	doc_bin = DocBin().from_disk("data/" + key + ".spacy")
	#print("JSON Data Loaded: ", key)

	count_by_dep = {}
	for doc in doc_bin.get_docs(nlp.vocab):
		for token in doc:
			if token.dep_ not in count_by_dep:
				count_by_dep[token.dep_] = []
			count_by_dep[token.dep_].append(token.text.lower())

	dep_count = {}

	for dep in count_by_dep:
		dep_freq = Counter(count_by_dep[dep])
		common_dep = dep_freq.most_common()

		dep_count[dep] = {}
		dep_count[dep]["month_key"] = key
		for dep_entry in common_dep:
			if dep_entry[1] > 1:
				dep_count[dep][dep_entry[0]] = dep_entry[1]

	return dep_count

if __name__ == '__main__':

	p = Pool(12)

	jobs = []

	for line in open('file_index', 'r').readlines():
		words = nltk.word_tokenize(line.strip().replace("/", " ").replace("_", " ").replace(".", " "))
		key = str(datetime.date(int(words[1]), int(words[2]), 1))
		print("Processing file: ", key)
		jobs.append(key)

	time.sleep(2)

	results = p.map(process_function, jobs)
	p.close()
	p.join()

	dfs = {}
	print("Putting results into data frame")
	for result in results:
		for key in result:
			if key not in dfs:
				dfs[key] = []
			dfs[key].append(pd.DataFrame([result[key]]))

	for key in dfs:
		print("Key: ", key)
		print("Concatenating results")
		df = pd.concat(dfs[key], ignore_index = True)
		
		print("Sorting the results")
		df = df.sort_values(by=["month_key"], ascending = True)

		print("Seting the index")
		df = df.set_index('month_key')

		print("Removing zeros")
		df = df.fillna(0)
	
		print("Saving results to pickle")
		df.to_pickle("pickles/" + key + "_dep_data.pickle")

	print("Jobs finished")
