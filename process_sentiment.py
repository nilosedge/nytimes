import json
import time
import datetime
import nltk # nltk.download('punkt')

from multiprocessing import Pool
import pandas as pd

import spacy
from spacy.tokens import Doc
from spacy.tokens import DocBin
from spacy.matcher import Matcher

from collections import Counter


nlp = spacy.load("en_core_web_lg")
nlp.max_length = 2500000

matcher1 = Matcher(nlp.vocab)
matcher2 = Matcher(nlp.vocab)

pattern3 = [{"LOWER": "republicans"}]
pattern4 = [{"LOWER": "democrats"}]

matcher1.add("republicans", [pattern3])
matcher2.add("democrats", [pattern4])

Doc.set_extension("month_key", default=None)
Doc.set_extension("polarity", default=None)

def process_function(key):

	print("Process starting: ", key)
	#print("Loading JSON File: ", filename)		   
	doc_bin = DocBin().from_disk("data/" + key + ".spacy")
	#print("JSON Data Loaded: ", key)

	bias_counts = None
	for doc in doc_bin.get_docs(nlp.vocab):
		if not bias_counts:
			bias_counts = { "month_key": doc._.month_key, "rs": 0, "rc": 0, "ds": 0, "dc": 0 }

		matches1 = matcher1(doc)
		matches2 = matcher2(doc)
		if doc._.polarity != 0:
			if matches1 and matches2:
				bias_counts["rs"] += doc._.polarity;
				bias_counts["rc"] += 1;
				bias_counts["ds"] += doc._.polarity;
				bias_counts["dc"] += 1;
			elif matches1:
				bias_counts["rs"] += doc._.polarity;
				bias_counts["rc"] += 1;
			elif matches2:
				bias_counts["ds"] += doc._.polarity;
				bias_counts["dc"] += 1;

	return bias_counts

if __name__ == '__main__':

	p = Pool()

	jobs = []

	for line in open('file_index', 'r').readlines():
		words = nltk.word_tokenize(line.strip().replace("/", " ").replace("_", " ").replace(".", " "))
		key = str(datetime.date(int(words[1]), int(words[2]), 1))
		print("Processing file: ", key)
		jobs.append(key)

	time.sleep(2)

	results = p.map(process_function, jobs)
	p.close()
	p.join()

	dfs = []
	for result in results:
		dfs.append(pd.DataFrame([result]))

	print("Concatenating results")
	df = pd.concat(dfs, ignore_index = True)

	print("Sorting the results")
	df = df.sort_values(by=["month_key"], ascending = True)

	print("Seting the index")
	df = df.set_index('month_key')

	df["ra"] = df["rs"] / df["rc"]
	df["da"] = df["ds"] / df["dc"]

	print("Removing zeros")
	df = df.fillna(0)
	
	print("Saving results to pickle")
	df.to_pickle("pickles/sentiment_data.pickle")


	print("Jobs finished")
