import os
import json
import time
import datetime
import nltk # nltk.download('punkt')
from multiprocessing import Pool

import spacy
from spacy.tokens import Doc
from spacy.tokens import DocBin
from spacytextblob.spacytextblob import SpacyTextBlob

nlp = spacy.load("en_core_web_lg")
nlp.max_length = 2500000

nlp2 = spacy.load("en_core_web_lg")
nlp2.add_pipe('spacytextblob')

Doc.set_extension("month_key", default=None)

def process_function(job):
	key = job[0]
	if os.path.exists("data/" + str(key) + ".spacy"):
		print("File Already exists not processing: ", key)
		return
	filename = job[1]
	print("Process starting: ", key)
	#print("Loading JSON File: ", filename)		   
	data = json.load(open(filename, 'r'))
	#print("JSON Data Loaded: ", key)		

	headlines = []	   
	for doc in data['response']['docs']:
		#print(doc['headline']['main'])
		headlines.append(doc['headline']['main'])
	
	#print("Processing Data: ", key)
	doc_list = list(nlp.pipe(headlines))
	for doc in doc_list:
		doc2 = nlp2(doc.text)
		doc._.month_key = str(key)
		doc._.polarity = doc2._.blob.polarity
	#print("Processing Finished: ", key)
	doc_bin = DocBin(docs=doc_list, store_user_data = True)
	print("Doc Bin Creation finished: ", key)
	doc_bin.to_disk("data/" + str(key) + ".spacy")
	print("Doc Bin to disk finished: ", key)


if __name__ == '__main__':

	p = Pool()

	jobs = []

	for line in open('file_index', 'r').readlines():
		words = nltk.word_tokenize(line.strip().replace("/", " ").replace("_", " ").replace(".", " "))
		key = datetime.date(int(words[1]), int(words[2]), 1)
		print("Queuing file: ", key)
		jobs.append([key, line.strip()])

	time.sleep(2)

	p.map(process_function, jobs)
	p.close()
	p.join()

	print("Jobs finished")
